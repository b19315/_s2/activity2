package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayArrayListHashMap {

    public static void main(String[] args){

        int[] intArray = {2, 4, 6,8, 10};
        System.out.println("The first Prime number is:" + " " + intArray[0]);

        ArrayList<String> friends = new ArrayList<>();

        //adding elements using .add
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are:" + " " + friends);

        HashMap<String, Integer> supply = new HashMap<>();

        supply.put("toothpaste", 15);
        supply.put("toothbrush", 20);
        supply.put("soap", 12);

        System.out.println("Our current inventory consist of:" + " " + supply);

    }

}
